//
//  SearchResultDetailViewController.swift
//  Abendi
//
//  Created by Gilson Gil on 3/10/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SearchResultDetailViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  
  var snqc: Snqc!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.registerNib(UINib(nibName: "Header", bundle: nil), forHeaderFooterViewReuseIdentifier: "Header")
    getCertificates()
  }
  
  func getCertificates() {
    NetworkManager.request(Router.Certificates(String(snqc.snqc))) { result in
      SVProgressHUD.dismiss()
      switch result {
      case .Success(let boxed):
        if let certificates = boxed.unbox as? [Certificate] {
          self.snqc.certificates = certificates.filter() {
            let validStatuses = ["I1", "I2", "I3", "I4", "I5", "I6", "E13"]
            return find(validStatuses, $0.status) != nil
          }
        }
      case .Failure(let error):
        break
      }
    }
  }
}
