//
//  LoginViewController.swift
//  Abendi
//
//  Created by Gilson Gil on 3/10/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
  @IBOutlet weak var usernameTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var errorLabel: UILabel!
  
  var loginCallback: (() -> ())?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureView()
    let tap = UITapGestureRecognizer(target: self, action: "tapped")
    view.addGestureRecognizer(tap)
  }
  
  func configureView() {
    usernameTextField.setPaddings()
    passwordTextField.setPaddings()
  }
  
  @IBAction func login(sender: UIButton) {
    login()
  }
  
  func tapped() {
    usernameTextField.resignFirstResponder()
    passwordTextField.resignFirstResponder()
  }
  
  func login() {
    if validateFields() {
      SVProgressHUD.showWithMaskType(.Gradient)
      NetworkManager.request(Router.Login(username: usernameTextField.text, password: passwordTextField.text)) { result in
        SVProgressHUD.dismiss()
        switch result {
        case .Success(let boxed):
          if let dict = boxed.unbox as? [String: String], let login = dict["login"] where count(login) > 0, let token = dict["token"] where count(token) > 0, let loginCallback = self.loginCallback, let name = dict["name"] {
            SSKeychain.setPassword(login, forService: "abendiapp", account: "UserId")
            SSKeychain.setPassword(token, forService: "abendiapp", account: "Token")
            SSKeychain.setPassword(name, forService: "abendiapp", account: "Name")
            SSKeychain.setPassword(self.usernameTextField.text, forService: "abendiapp", account: "Login")
            SSKeychain.setPassword(self.passwordTextField.text, forService: "abendiapp", account: "Password")
            loginCallback()
          } else {
            self.errorLabel.hidden = false
          }
        case .Failure(let error):
          self.errorLabel.hidden = false
        }
      }
      errorLabel.hidden = true
    } else {
      errorLabel.hidden = false
    }
  }
  
  func validateFields() -> Bool {
    return count(usernameTextField.text) > 0 && count(passwordTextField.text) > 0
  }
}
