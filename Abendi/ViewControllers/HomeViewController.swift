//
//  HomeViewController.swift
//  Abendi
//
//  Created by Gilson Gil on 3/10/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
  @IBOutlet weak var restrictButton: UIButton!
  @IBOutlet weak var searchButton: UIButton!
  @IBOutlet weak var aboutButton: UIButton!
  @IBOutlet weak var containerView: UIView!
  
  var restrictViewController: UIViewController!
  var searchViewController: SearchViewController!
  var aboutViewController: UIViewController!
  weak var currentViewController: UIViewController?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configure()
  }
  
  // MARK: View Container
  func configure() {
    searchViewController = storyboard!.instantiateViewControllerWithIdentifier("SearchViewController") as! SearchViewController
    searchViewController.view.frame = containerView.bounds
    searchViewController.delegate = self
    selectViewController(searchViewController)
    searchButton.setTabSelected(true)
  }
  
  func selectViewController(viewController: UIViewController) {
    if viewController != currentViewController {
      currentViewController = viewController
      currentViewController!.willMoveToParentViewController(self)
      containerView.addSubview(currentViewController!.view)
      currentViewController!.didMoveToParentViewController(self)
    }
  }
  
  @IBAction func restrictTapped(sender: UIButton?) {
    if sender != nil && sender!.selected { return }
    if restrictViewController == nil {
      if SSKeychain.passwordForService("abendiapp", account: "UserId") != nil && count(SSKeychain.passwordForService("abendiapp", account: "UserId")!) > 0 {
        restrictViewController = storyboard!.instantiateViewControllerWithIdentifier("RestrictViewController") as! RestrictViewController
        (restrictViewController as! RestrictViewController).delegate = self
      } else {
        restrictViewController = storyboard!.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
        (restrictViewController as! LoginViewController).loginCallback = {
          self.restrictViewController = nil
          self.restrictTapped(nil)
        }
      }
    }
    selectViewController(restrictViewController)
    restrictButton.setTabSelected(true)
    searchButton.setTabSelected(false)
    aboutButton.setTabSelected(false)
  }
  
  @IBAction func searchTapped(sender: UIButton) {
    if sender.selected { return }
    selectViewController(searchViewController)
    restrictButton.setTabSelected(false)
    searchButton.setTabSelected(true)
    aboutButton.setTabSelected(false)
  }
  
  @IBAction func aboutTapped(sender: UIButton) {
    if sender.selected { return }
    if aboutViewController == nil {
      aboutViewController = storyboard!.instantiateViewControllerWithIdentifier("AboutViewController") as! UIViewController
    }
    selectViewController(aboutViewController)
    restrictButton.setTabSelected(false)
    searchButton.setTabSelected(false)
    aboutButton.setTabSelected(true)
  }
  
  func pushViewController(viewController: UIViewController) {
    navigationController?.pushViewController(viewController, animated: true)
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
  }
}

// MARK: SearchViewController Delegate
extension HomeViewController: SearchViewControllerDelegate {
  func searchWithResults(results: [Snqc]) {
    if results.count > 0 {
      if let searchResultsViewController = UIStoryboard(name: "Search", bundle: nil).instantiateInitialViewController() as? SearchResultsViewController {
        searchResultsViewController.results = results
        pushViewController(searchResultsViewController)
      }
    } else {
      showNoResult()
    }
  }
  
  func searchWithSnqc(snqc: Snqc) {
    if let searchResultDetailViewController = UIStoryboard(name: "Search", bundle: nil).instantiateViewControllerWithIdentifier("SearchResultDetailViewController") as? SearchResultDetailViewController {
      searchResultDetailViewController.snqc = snqc
      pushViewController(searchResultDetailViewController)
    }
  }
  
  func showNoResult() {
    if let noResultViewController = UIStoryboard(name: "Search", bundle: nil).instantiateViewControllerWithIdentifier("NoResultViewController") as? UIViewController {
      pushViewController(noResultViewController)
    }
  }
  
  func qrCode() {
    if let qrCodeViewController = UIStoryboard(name: "Search", bundle: nil).instantiateViewControllerWithIdentifier("QRCodeViewController") as? QRCodeViewController {
      qrCodeViewController.callback = { snqc in
        self.navigationController?.popToRootViewControllerAnimated(true)
        self.searchViewController.searchWithSnqcNumber(snqc)
      }
      qrCodeViewController.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
      pushViewController(qrCodeViewController)
    }
  }
}

extension HomeViewController: RestrictViewControllerDelegate {
  func goToAgenda() {
    if let agendaViewController = UIStoryboard(name: "Restrict", bundle: nil).instantiateViewControllerWithIdentifier("AgendaViewController") as? AgendaViewController {
      pushViewController(agendaViewController)
    }
  }
  
  func goToGrades() {
    if let gradesViewController = UIStoryboard(name: "Restrict", bundle: nil).instantiateViewControllerWithIdentifier("GradesViewController") as? GradesViewController {
      pushViewController(gradesViewController)
    }
  }
  
  func goToCertificates() {
    if let certificatesViewController = UIStoryboard(name: "Restrict", bundle: nil).instantiateViewControllerWithIdentifier("CertificatesViewController") as? CertificatesViewController {
      pushViewController(certificatesViewController)
    }
  }
  
  func goToInvoices() {
    if let paymentViewController = UIStoryboard(name: "Restrict", bundle: nil).instantiateViewControllerWithIdentifier("PaymentViewController") as? PaymentViewController {
      pushViewController(paymentViewController)
    }
  }
  
  func logout() {
    SSKeychain.deletePasswordForService("abendiapp", account: "UserId")
    restrictViewController = nil
    restrictTapped(nil)
  }
}

// MARK: Button Extension
extension UIButton {
  func setTabSelected(selected: Bool) {
    self.selected = selected
    if selected {
      backgroundColor = UIColor.selectedTabbarColor()
    } else {
      backgroundColor = UIColor.tabbarColor()
    }
  }
}
