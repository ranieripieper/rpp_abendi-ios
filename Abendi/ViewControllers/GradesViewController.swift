//
//  GradesViewController.swift
//  Abendi
//
//  Created by Gilson Gil on 3/10/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class GradesViewController: UIViewController {
  @IBOutlet weak var filterView: FilterView!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var gradesTableViewDelegate: GradesTableViewDelegate!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    filterView.delegate = self
    configureView()
    refresh()
  }
  
  func configureView() {
    tableView.registerNib(UINib(nibName: "Header", bundle: nil), forHeaderFooterViewReuseIdentifier: "Header")
  }
  
  func refresh() {
    SVProgressHUD.showWithMaskType(.Gradient)
    SVProgressHUD.showWithMaskType(.Gradient)
    SVProgressHUD.showWithMaskType(.Gradient)
    if var userId = SSKeychain.passwordForService("abendiapp", account: "UserId") {
      NetworkManager.request(Router.GradesTheoric(userId)) { result in
        SVProgressHUD.dismiss()
        switch result {
        case .Success(let boxed):
          self.gradesTableViewDelegate.datasource = [(Type.Theoric, boxed.unbox as! [Grade]), self.gradesTableViewDelegate.datasource[1], self.gradesTableViewDelegate.datasource[2]]
        case .Failure(let error):
          break
        }
        self.tableView.reloadData()
      }
      NetworkManager.request(Router.GradesPractical(userId)) { result in
        SVProgressHUD.dismiss()
        switch result {
        case .Success(let boxed):
          self.gradesTableViewDelegate.datasource = [self.gradesTableViewDelegate.datasource[0], (Type.Practical, boxed.unbox as! [Grade]), self.gradesTableViewDelegate.datasource[2]]
        case .Failure(let error):
          break
        }
        self.tableView.reloadData()
      }
      NetworkManager.request(Router.GradesRecertification(userId)) { result in
        SVProgressHUD.dismiss()
        switch result {
        case .Success(let boxed):
          self.gradesTableViewDelegate.datasource = [self.gradesTableViewDelegate.datasource[0], self.gradesTableViewDelegate.datasource[1], (Type.Recertification, boxed.unbox as! [Grade])]
        case .Failure(let error):
          break
        }
        self.tableView.reloadData()
      }
    }
  }
}

extension GradesViewController: FilterViewDelegate {
  func filterView(filterView: FilterView, didSelectType type: Type) {
    gradesTableViewDelegate.filteredDatasource = gradesTableViewDelegate.datasource.filter() {
      if type == .All {
        return true
      } else {
        return $0.0 == type
      }
    }
    tableView.reloadData()
  }
}
