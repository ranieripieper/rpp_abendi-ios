//
//  AgendaViewController.swift
//  Abendi
//
//  Created by Gilson Gil on 3/10/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class AgendaViewController: UIViewController {
  @IBOutlet weak var filterView: FilterView!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var agendaTableViewDelegate: AgendaTableViewDelegate!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    filterView.delegate = self
    configureView()
    refresh()
  }
  
  func configureView() {
    tableView.registerNib(UINib(nibName: "Header", bundle: nil), forHeaderFooterViewReuseIdentifier: "Header")
  }
  
  func refresh() {
    SVProgressHUD.showWithMaskType(.Gradient)
    SVProgressHUD.showWithMaskType(.Gradient)
    SVProgressHUD.showWithMaskType(.Gradient)
    if var userId = SSKeychain.passwordForService("abendiapp", account: "UserId") {
      NetworkManager.request(Router.AgendasTheoric(userId)) { result in
        SVProgressHUD.dismiss()
        switch result {
        case .Success(let boxed):
          self.agendaTableViewDelegate.datasource = [(Type.Theoric, boxed.unbox as! [Agenda]), self.agendaTableViewDelegate.datasource[1], self.agendaTableViewDelegate.datasource[2]]
        case .Failure(let error):
          break
        }
        self.tableView.reloadData()
      }
      NetworkManager.request(Router.AgendasPractical(userId)) { result in
        SVProgressHUD.dismiss()
        switch result {
        case .Success(let boxed):
          self.agendaTableViewDelegate.datasource = [self.agendaTableViewDelegate.datasource[0], (Type.Practical, boxed.unbox as! [Agenda]), self.agendaTableViewDelegate.datasource[2]]
        case .Failure(let error):
          break
        }
        self.tableView.reloadData()
      }
      NetworkManager.request(Router.AgendasRecertification(userId)) { result in
        SVProgressHUD.dismiss()
        switch result {
        case .Success(let boxed):
          self.agendaTableViewDelegate.datasource = [self.agendaTableViewDelegate.datasource[0], self.agendaTableViewDelegate.datasource[1], (Type.Recertification, boxed.unbox as! [Agenda])]
        case .Failure(let error):
          break
        }
        self.tableView.reloadData()
      }
    }
  }
}

extension AgendaViewController: FilterViewDelegate {
  func filterView(filterView: FilterView, didSelectType type: Type) {
    agendaTableViewDelegate.filteredDatasource = agendaTableViewDelegate.datasource.filter() {
      if type == .All {
        return true
      } else {
        return $0.0 == type
      }
    }
    tableView.reloadData()
  }
}
