//
//  InvoicesViewController.swift
//  Abendi
//
//  Created by Gilson Gil on 3/10/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class InvoicesViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var invoiceTableViewDelegate: InvoiceTableViewDelegate!
  
  let noResultsText = "Não encontramos nenhum pagamento pendente."
  
  override func viewDidLoad() {
    super.viewDidLoad()
    getInvoices()
    tableView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
  }
  
  func getInvoices() {
    SVProgressHUD.showWithMaskType(.Gradient)
    if var userId = SSKeychain.passwordForService("abendiapp", account: "UserId") {
      NetworkManager.request(Router.Invoices(userId)) { result in
        SVProgressHUD.dismiss()
        switch result {
        case .Success(let boxed):
          self.invoiceTableViewDelegate.datasource = boxed.unbox as! [Invoice]
          if boxed.unbox.count == 0 {
            self.presentNoResultAlert()
          }
        case .Failure(let error):
          self.presentNoResultAlert()
        }
        self.tableView.reloadData()
      }
    }
  }
  
  func presentNoResultAlert() {
    tableView.hidden = true
    let labelPadding: CGFloat = 40
    let labelHeight: CGFloat = 100
    let label = UILabel(frame: CGRect(x: labelPadding, y: (view.bounds.height - labelHeight) / 2, width: view.bounds.width - labelPadding * 2, height: labelHeight))
    label.text = noResultsText
    label.textColor = UIColor.whiteColor()
    label.backgroundColor = UIColor.clearColor()
    label.font = UIFont(name: "Arial", size: 20)
    label.numberOfLines = 0
    view.addSubview(label)
  }
  
  func payInvoice(invoice: Invoice) {
    performSegueWithIdentifier("SeguePay", sender: invoice)
  }
}
