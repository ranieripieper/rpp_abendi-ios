//
//  PaymentViewController.swift
//  Abendi
//
//  Created by Gilson Gil on 4/17/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController {
  @IBOutlet weak var webView: UIWebView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    SVProgressHUD.showWithMaskType(.Gradient)
    login() {
      switch $0 {
      case .Success(let boxed):
        self.request(boxed.unbox)
      case .Failure(let error):
        SVProgressHUD.dismiss()
      }
    }
  }
  
  func login(completion: Result<String> -> ()) {
    if let login = SSKeychain.passwordForService("abendiapp", account: "Login"), let password = SSKeychain.passwordForService("abendiapp", account: "Password") {
      NetworkManager.request(Router.Login(username: login, password: password)) {
        switch $0 {
        case .Success(let boxed):
          if let dict = boxed.unbox as? [String: String], let token = dict["token"] where count(token) > 0 {
            SSKeychain.setPassword(token, forService: "abendiapp", account: "Token")
            completion(Result(token))
          }
        case .Failure(let error):
          completion(Result(error))
        }
      }
    }
  }
  
  func request(token: String) {
    let request = NSURLRequest(URL: NSURL(string: "http://www.abendi.org.br/abendi/pgto_app.aspx?token=\(token)")!)
    webView.loadRequest(request)
  }
}

extension PaymentViewController: UIWebViewDelegate {
  func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
    SVProgressHUD.dismiss()
  }
  
  func webViewDidFinishLoad(webView: UIWebView) {
    SVProgressHUD.dismiss()
  }
}
