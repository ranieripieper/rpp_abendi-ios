//
//  RestrictViewController.swift
//  Abendi
//
//  Created by Gilson Gil on 3/10/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

protocol RestrictViewControllerDelegate {
  func goToAgenda()
  func goToGrades()
  func goToCertificates()
  func goToInvoices()
  func logout()
}

class RestrictViewController: UIViewController {
  @IBOutlet weak var userLabel: UILabel!
  @IBOutlet weak var agendaGradesVerticalConstraint: NSLayoutConstraint!
  @IBOutlet weak var gradesCertificatesVerticalConstraint: NSLayoutConstraint!
  @IBOutlet weak var certificatesInvoicesVerticalConstraint: NSLayoutConstraint!
  
  var delegate: RestrictViewControllerDelegate?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if let user = SSKeychain.passwordForService("abendiapp", account: "Name") {
      userLabel.text = "Olá, \(user)!"
    }
    if UIScreen.mainScreen().bounds.height == 480 {
      agendaGradesVerticalConstraint.constant = 6
      gradesCertificatesVerticalConstraint.constant = 6
      certificatesInvoicesVerticalConstraint.constant = 6
    }
  }
  
  @IBAction func agendaTapped(sender: UIButton) {
    delegate?.goToAgenda()
  }
  
  @IBAction func gradesTapped(sender: UIButton) {
    delegate?.goToGrades()
  }
  
  @IBAction func certificatesTapped(sender: UIButton) {
    delegate?.goToCertificates()
  }
  
  @IBAction func invoicesTapped(sender: UIButton) {
    delegate?.goToInvoices()
  }
  
  @IBAction func logoutTapped(sender: UIButton) {
    delegate?.logout()
  }
}
