//
//  QRCodeViewController.swift
//  Abendi
//
//  Created by Gilson Gil on 3/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class QRCodeViewController: UIViewController {
  @IBOutlet weak var qrCodeView: QRCode!
  
  var read = false
  var callback: (String -> ())?
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    qrCodeView.callback = searchWithQRCode
    qrCodeView.startReading()
    read = false
  }
  
  func searchWithQRCode(string: String) {
    let substring = string.stringByReplacingOccurrencesOfString("http://www.abendi.org.br/abendi//default.aspx?c=snqc_consultapublica&ec=declaracao&id=", withString: "")
    if count(substring) > 0 && !read {
      read = true
      qrCodeView.stopReading()
      callback?(substring)
    }
  }
}
