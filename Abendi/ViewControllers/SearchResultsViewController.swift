//
//  SearchResultsViewController.swift
//  Abendi
//
//  Created by Gilson Gil on 3/10/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SearchResultsViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var searchResultsTableViewDelegate: SearchResultsTableViewDelegate!
  
  var results: [Snqc]?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.registerNib(UINib(nibName: "SearchResultsHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "SearchResultsHeader")
    tableView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
    tableView.scrollIndicatorInsets = tableView.contentInset
    searchResultsTableViewDelegate.datasource = results ?? []
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    if let indexPath = tableView.indexPathForSelectedRow() {
      tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
  }
  
  func searchSnqc(snqc: Snqc) {
    SVProgressHUD.showWithMaskType(.Gradient)
    NetworkManager.request(Router.Search(type: "snqc", value: String(snqc.snqc))) { result in
      SVProgressHUD.dismiss()
      switch result {
      case .Success(let boxed):
        if let newSnqc = boxed.unbox as? Snqc where newSnqc.snqc != 0 {
          self.performSegueWithIdentifier("SegueResultDetail", sender: newSnqc)
        } else {
          self.performSegueWithIdentifier("SegueResultDetail", sender: snqc)
        }
      case .Failure(let error):
        self.performSegueWithIdentifier("SegueResultDetail", sender: snqc)
      }
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Bordered, target: nil, action: nil)
    if let searchResultDetailViewController = segue.destinationViewController as? SearchResultDetailViewController, let snqc = sender as? Snqc {
      searchResultDetailViewController.snqc = snqc
    }
  }
}
