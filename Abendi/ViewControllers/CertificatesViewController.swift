//
//  CertificatesViewController.swift
//  Abendi
//
//  Created by Gilson Gil on 3/10/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CertificatesViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var certificatesTableViewDelegate: CertificatesTableViewDelegate!
  
  let noResultsText = "Não constam registros referente a Extrato das Certificações\n\nEm caso de dúvida favor contatar Secretaria do Bureau."
  
  override func viewDidLoad() {
    super.viewDidLoad()
    getCertificates()
  }
  
  func getCertificates() {
    SVProgressHUD.showWithMaskType(.Gradient)
    if var userId = SSKeychain.passwordForService("abendiapp", account: "UserId") {
      NetworkManager.request(Router.Certificates(userId)) { result in
        SVProgressHUD.dismiss()
        switch result {
        case .Success(let boxed):
          self.certificatesTableViewDelegate.datasource = boxed.unbox as! [Certificate]
          if boxed.unbox.count == 0 {
            self.presentNoResultAlert()
          }
        case .Failure(let error):
          self.presentNoResultAlert()
        }
        self.tableView.reloadData()
      }
    }
  }
  
  func presentNoResultAlert() {
    tableView.hidden = true
    let labelPadding: CGFloat = 20
    let labelHeight: CGFloat = 140
    let label = UILabel(frame: CGRect(x: labelPadding, y: 144, width: 260, height: labelHeight))
    label.text = noResultsText
    label.textColor = UIColor.appLightTextColor()
    label.backgroundColor = UIColor.clearColor()
    label.font = UIFont(name: "Arial-ItalicMT", size: 16)
    label.numberOfLines = 0
    view.addSubview(label)
  }
}
