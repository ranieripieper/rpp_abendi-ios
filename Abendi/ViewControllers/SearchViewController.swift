//
//  SearchViewController.swift
//  Abendi
//
//  Created by Gilson Gil on 3/10/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

protocol SearchViewControllerDelegate {
  func searchWithResults(results: [Snqc])
  func searchWithSnqc(snqc: Snqc)
  func qrCode()
  func showNoResult()
}

class SearchViewController: UIViewController {
  @IBOutlet weak var snqcTextField: UITextField!
  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var techniqueTextField: UITextField!
  @IBOutlet weak var tap: UITapGestureRecognizer!
  
  var delegate: SearchViewControllerDelegate?
  var alertView: AlertController?
  var techniques: [String]?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureView()
    getTechniques()
  }
  
  func configureView() {
    snqcTextField.setPaddings()
    nameTextField.setPaddings()
    techniqueTextField.setPaddings()
    view.addGestureRecognizer(tap)
    let pickerView = UIPickerView()
    pickerView.dataSource = self
    pickerView.delegate = self
    techniqueTextField.inputView = pickerView
  }
  
  @IBAction func didTap(sender: UITapGestureRecognizer) {
    snqcTextField.resignFirstResponder() || nameTextField.resignFirstResponder() || techniqueTextField.resignFirstResponder()
  }
  
  @IBAction func searchTapped(sender: UIButton?) {
    if count(snqcTextField.text) > 0 {
      searchWithSnqcNumber(snqcTextField.text)
    } else if count(nameTextField.text) > 0 {
      searchForType("nome", value: nameTextField.text)
    } else if count(techniqueTextField.text) > 0 {
      searchForType("tecnica", value: techniqueTextField.text)
    } else {
      alertView = AlertController(title: "", message: "Por favor, digite o que está procurando.", buttons: nil, cancelButton: ("Ok", {
        self.alertView = nil
      }), style: .Alert)
      alertView!.alertInViewController(self)
    }
  }
  
  func getTechniques() {
    SVProgressHUD.showWithMaskType(.Gradient)
    NetworkManager.request(Router.ListTechniques) {
      SVProgressHUD.dismiss()
      switch $0 {
      case .Success(let boxed):
        self.techniques = boxed.unbox as? [String]
      case .Failure(let error):
        break
      }
    }
  }
  
  func searchWithSnqcNumber(snqc: String) {
    SVProgressHUD.showWithMaskType(.Gradient)
    NetworkManager.request(Router.Search(type: "snqc", value: snqc)) { result in
      SVProgressHUD.dismiss()
      switch result {
      case .Success(let boxed):
        if let snqc = boxed.unbox as? Snqc where snqc.snqc != 0 {
          self.delegate?.searchWithSnqc(snqc)
        } else {
          self.delegate?.showNoResult()
        }
      case .Failure(let error):
        self.delegate?.showNoResult()
      }
    }
  }
  
  func searchForType(type: String, value: String) {
    SVProgressHUD.showWithMaskType(.Gradient)
    NetworkManager.request(Router.Search(type: type, value: value)) { result in
      SVProgressHUD.dismiss()
      switch result {
      case .Success(let boxed):
        self.delegate?.searchWithResults(boxed.unbox as! [Snqc])
      case .Failure(let error):
        self.delegate?.showNoResult()
      }
    }
  }
  
  @IBAction func qrCodeTapped(sender: UIButton) {
    delegate?.qrCode()
  }
}

extension SearchViewController: UITextFieldDelegate {
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    if textField != snqcTextField {
      snqcTextField.text = ""
    }
    if textField != nameTextField {
      nameTextField.text = ""
    }
    if textField != techniqueTextField {
      techniqueTextField.text = ""
    }
    return true
  }
  
  func textFieldDidBeginEditing(textField: UITextField) {
    if textField != snqcTextField {
      snqcTextField.text = ""
    }
    if textField != nameTextField {
      nameTextField.text = ""
    }
    if textField != techniqueTextField {
      techniqueTextField.text = ""
    }
  }
  
  func textFieldDidEndEditing(textField: UITextField) {
    if let pickerView = textField.inputView as? UIPickerView {
      textField.text = techniques?[pickerView.selectedRowInComponent(0)]
    }
  }
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    searchTapped(nil)
    return true
  }
}

extension SearchViewController: UIPickerViewDataSource, UIPickerViewDelegate {
  func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return techniques?.count ?? 0
  }

  func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
    return techniques?[row]
  }
  
  func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    techniqueTextField.text = techniques?[row]
  }
}
