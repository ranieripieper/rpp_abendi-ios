//
//  Header.swift
//  Abendi
//
//  Created by Gilson Gil on 3/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class Header: UITableViewHeaderFooterView {
  @IBOutlet weak var typeLabel: UILabel!
  
  func configureWithText(text: String) {
    typeLabel.text = text
  }
}
