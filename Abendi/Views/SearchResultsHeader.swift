//
//  SearchResultsHeader.swift
//  Abendi
//
//  Created by Gilson Gil on 3/10/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SearchResultsHeader: UITableViewHeaderFooterView {
  @IBOutlet weak var label: UILabel!
  var total: Int?
  
  func configureWithTotal(total: Int) {
    label.text = "\(total) registros para sua busca"
  }
}
