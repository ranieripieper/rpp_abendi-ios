//
//  FilterView.swift
//  Abendi
//
//  Created by Gilson Gil on 3/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

protocol FilterViewDelegate {
  func filterView(filterView: FilterView, didSelectType type: Type)
}

class FilterView: UIView {
  var allButton = UIButton.buttonWithTitle("Todos")
  var theoricButton = UIButton.buttonWithTitle("Teórico")
  var practicalButton = UIButton.buttonWithTitle("Prático")
  var recertificationButton = UIButton.buttonWithTitle("Recertificação")
  
  let indicatorImageViewWidth: CGFloat = 30
  let indicatorImageViewHeight: CGFloat = 10
  var indicatorImageView: UIImageView!
  
  var selectedType: Type = .All
  var delegate: FilterViewDelegate?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    allButton.frame = CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.width / 5, height: bounds.height - indicatorImageViewHeight)
    theoricButton.frame = CGRect(x: UIScreen.mainScreen().bounds.width / 5, y: 0, width: UIScreen.mainScreen().bounds.width / 5, height: bounds.height - indicatorImageViewHeight)
    practicalButton.frame = CGRect(x: UIScreen.mainScreen().bounds.width / 5 * 2, y: 0, width: UIScreen.mainScreen().bounds.width / 5, height: bounds.height - indicatorImageViewHeight)
    recertificationButton.frame = CGRect(x: UIScreen.mainScreen().bounds.width / 5 * 3, y: 0, width: UIScreen.mainScreen().bounds.width / 5 * 2, height: bounds.height - indicatorImageViewHeight)
    
    allButton.selected = true
    
    allButton.addTarget(self, action: "selectButton:", forControlEvents: .TouchUpInside)
    theoricButton.addTarget(self, action: "selectButton:", forControlEvents: .TouchUpInside)
    practicalButton.addTarget(self, action: "selectButton:", forControlEvents: .TouchUpInside)
    recertificationButton.addTarget(self, action: "selectButton:", forControlEvents: .TouchUpInside)

    instantiateIndicatorImageView()
    indicatorImageView.frame = CGRect(x: allButton.frame.origin.x + (allButton.frame.width - indicatorImageViewWidth) / 2, y: bounds.height - indicatorImageViewHeight, width: indicatorImageViewWidth, height: indicatorImageViewHeight)
    
    addSubview(allButton)
    addSubview(theoricButton)
    addSubview(practicalButton)
    addSubview(recertificationButton)
    addSubview(indicatorImageView)
  }
  
  func instantiateIndicatorImageView() {
    if indicatorImageView != nil { return }
    
    var indicatorImage: UIImage {
      let view = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: indicatorImageViewWidth, height: indicatorImageViewHeight)))
      UIGraphicsBeginImageContextWithOptions(view.frame.size, false, 0)
      let context = UIGraphicsGetCurrentContext()
      CGContextSetStrokeColorWithColor(context, UIColor.buttonBackgroundColor().CGColor)
      CGContextSetFillColorWithColor(context, UIColor.buttonBackgroundColor().CGColor)
      CGContextMoveToPoint(context, 0, 0)
      CGContextAddLineToPoint(context, indicatorImageViewWidth, 0)
      CGContextAddLineToPoint(context, indicatorImageViewWidth / 2, indicatorImageViewHeight)
      CGContextClosePath(context)
      CGContextFillPath(context)
      let image = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()
      return image
    }
    
    indicatorImageView = UIImageView(image: indicatorImage)
    indicatorImageView.frame = CGRect(x: UIScreen.mainScreen().bounds.width / 5 / 2 - indicatorImageViewHeight, y: bounds.height - indicatorImageViewHeight, width: indicatorImageViewWidth, height: indicatorImageViewHeight)
  }
  
  func selectButton(sender: UIButton) {
    if sender.selected { return }
    
    allButton.selected = false
    theoricButton.selected = false
    practicalButton.selected = false
    recertificationButton.selected = false
    
    animateIndicatorToButton(sender)
    
    switch sender {
    case allButton:
      selectedType = .All
    case theoricButton:
      selectedType = .Theoric
    case practicalButton:
      selectedType = .Practical
    case recertificationButton:
      selectedType = .Recertification
    default:
      break
    }
    delegate?.filterView(self, didSelectType: selectedType)
  }
  
  func animateIndicatorToButton(button: UIButton) {
    UIView.animateWithDuration(0.2) {
      button.selected = true
      self.indicatorImageView.frame = CGRect(x: button.frame.origin.x + (button.frame.width - self.indicatorImageViewWidth) / 2, y: self.bounds.height - self.indicatorImageViewHeight, width: self.indicatorImageViewWidth, height: self.indicatorImageViewHeight)
    }
  }
}
