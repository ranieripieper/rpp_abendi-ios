//
//  Agenda.swift
//  Abendi
//
//  Created by Gilson Gil on 3/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

enum Type {
  case All, Theoric, Practical, Recertification
  
  func toString() -> String {
    switch self {
    case .Theoric:
      return "Teórico"
    case .Practical:
      return "Prático"
    case .Recertification:
      return "Recertificação"
    default:
      return ""
    }
  }
  
  func toAgendaString() -> String {
    switch self {
    case .Theoric:
      return "AGENDA DE EXAMES DE QUALIFICAÇÃO - TEÓRICO"
    case .Practical:
      return "AGENDA DE EXAMES DE QUALIFICAÇÃO - PRÁTICO"
    case .Recertification:
      return "AGENDA DE EXAMES DE RECERTIFICAÇÃO"
    default:
      return ""
    }
  }
  
  func toAgendaDescriptionString() -> String {
    switch self {
    case .Theoric:
      return "Não constam registros referente a Agendamentos de Exames de Qualificação\n-Teórico\n\nEm caso de dúvida favor contatar\nSecretaria do Bureau."
    case .Practical:
      return "Não constam registros referente a Agendamentos de Exames de Qualificação\n-Prático\n\nEm caso de dúvida favor contatar\nSecretaria do Bureau."
    case .Recertification:
      return "Não constam registros referente a Agendamentos de Exames de Recertificação\n\nEm caso de dúvida favor contatar\nSecretaria do Bureau."
    default:
      return ""
    }
  }
  
  func toGradesString() -> String {
    switch self {
    case .Theoric:
      return "NOTAS DE EXAMES DE QUALIFICAÇÃO - TEÓRICO"
    case .Practical:
      return "NOTAS DE EXAMES DE QUALIFICAÇÃO - PRÁTICO"
    case .Recertification:
      return "NOTAS DE EXAMES DE RECERTIFICAÇÃO"
    default:
      return ""
    }
  }
  
  func toGradesDescriptionString() -> String {
    switch self {
    case .Theoric:
      return "Não constam registros referente a Notas de Exames de Qualificação\n-Teórico\n\nEm caso de dúvida favor contatar\nSecretaria do Bureau."
    case .Practical:
      return "Não constam registros referente a Notas de Exames de Qualificação\n-Prático\n\nEm caso de dúvida favor contatar\nSecretaria do Bureau."
    case .Recertification:
      return "Não constam registros referente a Notas de Exames de Recertificação\n\nEm caso de dúvida favor contatar\nSecretaria do Bureau."
    default:
      return ""
    }
  }
}

class Agenda {
  let technique: String
  let date: NSDate
  let time: String
  let location: String
  let type: Type

  init(technique: String, date: NSDate, time: String, location: String) {
    self.technique = technique
    self.date = date
    self.time = time
    self.location = location
    self.type = .All
  }
  
  init(_ dictionary: [String: AnyObject]) {
    self.technique = dictionary["codigo_tecnica"] as! String
    if let dateString = dictionary["data"] as? String, let timeInterval = dateString.substringToIndex(advance(dateString.startIndex, count(dateString) - 10)).substringFromIndex(advance(dateString.startIndex, 6)).toInt() {
      date = NSDate(timeIntervalSince1970: NSTimeInterval(timeInterval)) ?? NSDate(timeIntervalSince1970: 0)
    } else {
      date = NSDate(timeIntervalSince1970: 0)
    }
    self.time = dictionary["hora"] as! String
    self.location = dictionary["local"] as! String
    self.type = Type.All
  }
}
