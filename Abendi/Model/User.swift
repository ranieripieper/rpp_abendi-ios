//
//  User.swift
//  Abendi
//
//  Created by Gilson Gil on 3/10/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

class User {
  let snqc: String
  let name: String
  let techniques: [String]
  let expirationDate: NSDate
  
  init(snqc: String, name: String, techniques: [String], expirationDate: NSDate) {
    self.snqc = snqc
    self.name = name
    self.techniques = techniques
    self.expirationDate = expirationDate
  }
}
