//
//  Invoice.swift
//  Abendi
//
//  Created by Gilson Gil on 3/10/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

class Invoice {
  let id: Int
  let faceValue: String
  let description: String
  let expirationDate: NSDate
  
  init(_ dictionary: [String: AnyObject]) {
    self.id = dictionary["numero_sequencia"] as! Int
    self.faceValue = dictionary["valor_docto"] as! String
    self.description = dictionary["historico"] as! String
    if let dateString = dictionary["data_vencimento"] as? String, let timeInterval = dateString.substringToIndex(advance(dateString.startIndex, count(dateString) - 10)).substringFromIndex(advance(dateString.startIndex, 6)).toInt() {
      expirationDate = NSDate(timeIntervalSince1970: NSTimeInterval(timeInterval)) ?? NSDate(timeIntervalSince1970: 0)
    } else {
      expirationDate = NSDate(timeIntervalSince1970: 0)
    }
  }
}
