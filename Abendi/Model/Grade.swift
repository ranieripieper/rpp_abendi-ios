//
//  Grade.swift
//  Abendi
//
//  Created by Gilson Gil on 3/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

class Grade {
  let technique: String
  let date: NSDate
  let specificGrade: String?
  let generalGrade: String?
  let step1Grade: String?
  let step1AverageGrade: String?
  let step2Grade: String?
  let step2AverageGrade: String?
  let step3Grade: String?
  let step3AverageGrade: String?
  let grade1: String?
  let grade2: String?
  let grade: String
  let type: Type
  
  init(_ dictionary: [String: AnyObject]) {
    self.technique = dictionary["codigo_tecnica"] as! String
    if let dateString = dictionary["data"] as? String, let timeInterval = dateString.substringToIndex(advance(dateString.startIndex, count(dateString) - 10)).substringFromIndex(advance(dateString.startIndex, 6)).toInt() {
      date = NSDate(timeIntervalSince1970: NSTimeInterval(timeInterval)) ?? NSDate(timeIntervalSince1970: 0)
    } else {
      date = NSDate(timeIntervalSince1970: 0)
    }
    specificGrade = dictionary["nota_especifica"] as? String
    generalGrade = dictionary["nota_geral"] as? String
    step1Grade = dictionary["nota1_etapa1"] as? String
    step1AverageGrade = dictionary["media_etapa1"] as? String
    step2Grade = dictionary["nota1_etapa2"] as? String
    step2AverageGrade = dictionary["media_etapa2"] as? String
    step3Grade = dictionary["nota1_etapa3"] as? String
    step3AverageGrade = dictionary["media_etapa3"] as? String
    grade1 = dictionary["nota1_etapa1"] as? String
    grade2 = dictionary["nota2_etapa1"] as? String
    grade = dictionary["resultado"] as! String
    type = Type.All
  }
}
