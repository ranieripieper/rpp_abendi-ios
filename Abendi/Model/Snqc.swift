//
//  Snqc.swift
//  Abendi
//
//  Created by Gilson Gil on 4/16/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

class Snqc {
  let snqc: Int
  let name: String
  let location: String
  var certificates: [Certificate]
  
  init(_ dictionary: [String: AnyObject]) {
    snqc = dictionary["numero_snqc"] as! Int
    name = dictionary["nome"] as! String
    if let city = dictionary["cidade"] as? String, let state = dictionary["estado"] as? String {
      self.location = city + " - " + state
    } else {
      self.location = ""
    }
    certificates = [Certificate(dictionary)]
  }
  
  init(_ array: [[String: AnyObject]]) {
    if let first = array.first {
      snqc = first["numero_snqc"] as! Int
      name = first["nome"] as! String
      if let city = first["cidade"] as? String, let state = first["estado"] as? String {
        self.location = city + " - " + state
      } else {
        self.location = ""
      }
      certificates = array.map() {
        Certificate($0)
      }
    } else {
      snqc = 0
      name = ""
      location = ""
      certificates = []
    }
  }
  
  func currentCertificates() -> [Certificate] {
    return certificates.filter() { certificate in
      certificate.description.rangeOfString("Apto") != nil && certificate.description.rangeOfString("renovação") == nil
    }
  }
  
  func currentTraineeCertificates() -> [Certificate] {
    return certificates.filter() { certificate in
      false
    }
  }
  
  func recertificationRenovationCertificates() -> [Certificate] {
    return certificates.filter() { certificate in
      certificate.description.rangeOfString("Recertificação") != nil || certificate.description.rangeOfString("Renovação") != nil
    }
  }
  
  func admissionCertificates() -> [Certificate] {
    return certificates.filter() { certificate in
      certificate.description.rangeOfString("Reconhecimento") != nil
    }
  }
  
  func inProcessCertificates() -> [Certificate] {
    return certificates.filter() { certificate in
      certificate.description.rangeOfString("Processo") != nil
    }
  }
}
