//
//  Certificate.swift
//  Abendi
//
//  Created by Gilson Gil on 3/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

class Certificate {
  let technique: String
  let description: String
  let expiration: NSDate?
  let status: String
  
  init(_ dictionary: [String: AnyObject]) {
    technique = dictionary["codigo_tecnica"] as! String
    description = dictionary["status_descricao"] as? String ?? dictionary["situacao"] as? String ?? ""
    if let expireString = dictionary["validade"] as? String, let timeInterval = expireString.substringToIndex(advance(expireString.startIndex, count(expireString) - 10)).substringFromIndex(advance(expireString.startIndex, 6)).toInt() {
      expiration = NSDate(timeIntervalSince1970: NSTimeInterval(timeInterval)) ?? NSDate(timeIntervalSince1970: 0)
    } else {
      expiration = NSDate(timeIntervalSince1970: 0)
    }
    status = dictionary["status_codigo"] as? String ?? ""
  }
}
