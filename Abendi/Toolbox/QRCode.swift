//
//  QRCode.swift
//  Abendi
//
//  Created by Gilson Gil on 3/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import AVFoundation

class QRCode: UIView {
  typealias qrcodeCallback = (String) -> ()
  
  var captureSession: AVCaptureSession?
  weak var videoPreviewLayer: AVCaptureVideoPreviewLayer?
  var callback: qrcodeCallback?
  
  func startReading() -> Bool {
    var error: NSError?
    let captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
    if let input = AVCaptureDeviceInput.deviceInputWithDevice(captureDevice, error: &error) as? AVCaptureDeviceInput {
      captureSession = AVCaptureSession()
      captureSession!.addInput(input)
      
      let captureMetadataOutput = AVCaptureMetadataOutput()
      captureSession!.addOutput(captureMetadataOutput)
      
      let queue: dispatch_queue_t = dispatch_queue_create("qrcodequeue", nil)
      captureMetadataOutput.setMetadataObjectsDelegate(self, queue: queue)
      captureMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
      
      videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
      videoPreviewLayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
      videoPreviewLayer!.frame = bounds
      layer.addSublayer(videoPreviewLayer)
      
      captureSession!.startRunning()
      return true
    } else {
      return false
    }
  }
  
  func stopReading() {
    captureSession?.stopRunning()
    captureSession = nil
    videoPreviewLayer?.removeFromSuperlayer()
  }
}

extension QRCode: AVCaptureMetadataOutputObjectsDelegate {
  func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
    if metadataObjects?.count > 0 {
      if let metadataObject = metadataObjects.first as? AVMetadataMachineReadableCodeObject {
        if metadataObject.type == AVMetadataObjectTypeQRCode {
          dispatch_async(dispatch_get_main_queue()) {
            self.callback?(metadataObject.stringValue)
            return
          }
        }
      }
    }
  }
}
