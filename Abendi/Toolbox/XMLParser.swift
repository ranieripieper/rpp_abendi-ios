//
//  XMLParser.swift
//  Abendi
//
//  Created by Gilson Gil on 4/16/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class XMLParser: NSObject {
  var elementPrefix: String = ""
  var currentElementValue: String = ""
  
  init(_ elementPrefix: String) {
    self.elementPrefix = elementPrefix
  }
  
  var result: AnyObject {
    if let data = self.currentElementValue.dataUsingEncoding(NSUTF8StringEncoding), let json: AnyObject = NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments, error: nil) {
      return json
    }
    return []
  }
}

extension XMLParser: NSXMLParserDelegate {
  func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [NSObject : AnyObject]) {
    if elementName == "\(elementPrefix)JSONResult" {
      currentElementValue = ""
    }
  }
  
  func parser(parser: NSXMLParser, foundCharacters string: String?) {
    if let string = string {
      currentElementValue += string
    }
  }
}
