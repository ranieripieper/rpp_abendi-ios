//
//  NetworkManager.swift
//  Abendi
//
//  Created by Gilson Gil on 3/31/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class NetworkManager: AFHTTPRequestOperation {
  let domain = "br.com.abendi"
  let perPage = 20
}

typealias APIResponse = [String: AnyObject]

enum Router {
  static let soapURL = "http://www.abendi.org.br/abendi_ws/abendi.asmx"
//  static let soapURL = "http://www.abendihomologacao.com.br/abendi_ws/abendi.asmx"
  static var authToken: String?
  static let soapMessagePrefix = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"http://tempuri.org/\"> <SOAP-ENV:Body> "
  static let soapMessageSuffix = " </SOAP-ENV:Body> </SOAP-ENV:Envelope>"
  
  case Search(type: String, value: String)
  case ListTechniques
  case Login(username: String, password: String)
  case AgendasTheoric(String)
  case AgendasPractical(String)
  case AgendasRecertification(String)
  case GradesTheoric(String)
  case GradesPractical(String)
  case GradesRecertification(String)
  case Certificates(String)
  case Invoices(String)
  
  // MARK: NSURLRequest
  var URLRequest: NSURLRequest {
    let mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: Router.soapURL)!)
    mutableURLRequest.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
    mutableURLRequest.HTTPMethod = "POST"
    
    switch self {
    case .Search(let type, let value):
      let soapMessage = Router.soapMessagePrefix + "<ns1:SnqcConsultaPublicaJSON> <ns1:_tipo>\(type)</ns1:_tipo> <ns1:_valor>\(value)</ns1:_valor> </ns1:SnqcConsultaPublicaJSON>" + Router.soapMessageSuffix
      mutableURLRequest.addValue("http://tempuri.org/SnqcConsultaPublicaJSON", forHTTPHeaderField: "SOAPAction")
      mutableURLRequest.addValue(String(soapMessage.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)), forHTTPHeaderField: "Content-Length")
      mutableURLRequest.HTTPBody = soapMessage.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
    case .ListTechniques:
      let soapMessage = Router.soapMessagePrefix + "<ns1:SNQCListarTecnicasJSON />" + Router.soapMessageSuffix
      mutableURLRequest.addValue("http://tempuri.org/SNQCListarTecnicasJSON", forHTTPHeaderField: "SOAPAction")
      mutableURLRequest.addValue(String(soapMessage.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)), forHTTPHeaderField: "Content-Length")
      mutableURLRequest.HTTPBody = soapMessage.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
    case .Login(let username, let password):
      let soapMessage = Router.soapMessagePrefix + "<ns1:LogarJSON> <ns1:email>\(username)</ns1:email> <ns1:senha>\(password)</ns1:senha> </ns1:LogarJSON>" + Router.soapMessageSuffix
      mutableURLRequest.addValue("http://tempuri.org/LogarJSON", forHTTPHeaderField: "SOAPAction")
      mutableURLRequest.addValue(String(soapMessage.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)), forHTTPHeaderField: "Content-Length")
      mutableURLRequest.HTTPBody = soapMessage.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
    case .AgendasTheoric(let userId):
      let soapMessage = Router.soapMessagePrefix + "<ns1:SnqcConsultarAgendaExameTeoricoJSON> <ns1:numero_snqc>\(userId)</ns1:numero_snqc> </ns1:SnqcConsultarAgendaExameTeoricoJSON>" + Router.soapMessageSuffix
      mutableURLRequest.addValue("http://tempuri.org/SnqcConsultarAgendaExameTeoricoJSON", forHTTPHeaderField: "SOAPAction")
      mutableURLRequest.addValue(String(soapMessage.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)), forHTTPHeaderField: "Content-Length")
      mutableURLRequest.HTTPBody = soapMessage.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
    case .AgendasPractical(let userId):
      let soapMessage = Router.soapMessagePrefix + "<ns1:SnqcConsultarAgendaExamePraticoJSON> <ns1:numero_snqc>\(userId)</ns1:numero_snqc> </ns1:SnqcConsultarAgendaExamePraticoJSON>" + Router.soapMessageSuffix
      mutableURLRequest.addValue("http://tempuri.org/SnqcConsultarAgendaExamePraticoJSON", forHTTPHeaderField: "SOAPAction")
      mutableURLRequest.addValue(String(soapMessage.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)), forHTTPHeaderField: "Content-Length")
      mutableURLRequest.HTTPBody = soapMessage.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
    case .AgendasRecertification(let userId):
      let soapMessage = Router.soapMessagePrefix + "<ns1:SnqcConsultarAgendaExameRecertificacaoJSON> <ns1:numero_snqc>\(userId)</ns1:numero_snqc> </ns1:SnqcConsultarAgendaExameRecertificacaoJSON>" + Router.soapMessageSuffix
      mutableURLRequest.addValue("http://tempuri.org/SnqcConsultarAgendaExameRecertificacaoJSON", forHTTPHeaderField: "SOAPAction")
      mutableURLRequest.addValue(String(soapMessage.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)), forHTTPHeaderField: "Content-Length")
      mutableURLRequest.HTTPBody = soapMessage.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
    case .GradesTheoric(let userId):
      let soapMessage = Router.soapMessagePrefix + "<ns1:SnqcConsultarNotasExameTeoricoJSON> <ns1:numero_snqc>\(userId)</ns1:numero_snqc> </ns1:SnqcConsultarNotasExameTeoricoJSON>" + Router.soapMessageSuffix
      mutableURLRequest.addValue("http://tempuri.org/SnqcConsultarNotasExameTeoricoJSON", forHTTPHeaderField: "SOAPAction")
      mutableURLRequest.addValue(String(soapMessage.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)), forHTTPHeaderField: "Content-Length")
      mutableURLRequest.HTTPBody = soapMessage.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
    case .GradesPractical(let userId):
      let soapMessage = Router.soapMessagePrefix + "<ns1:SnqcConsultarNotasExamePraticoJSON> <ns1:numero_snqc>\(userId)</ns1:numero_snqc> </ns1:SnqcConsultarNotasExamePraticoJSON>" + Router.soapMessageSuffix
      mutableURLRequest.addValue("http://tempuri.org/SnqcConsultarNotasExamePraticoJSON", forHTTPHeaderField: "SOAPAction")
      mutableURLRequest.addValue(String(soapMessage.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)), forHTTPHeaderField: "Content-Length")
      mutableURLRequest.HTTPBody = soapMessage.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
    case .GradesRecertification(let userId):
      let soapMessage = Router.soapMessagePrefix + "<ns1:SnqcConsultarNotasExameRecertificacaoJSON> <ns1:numero_snqc>\(userId)</ns1:numero_snqc> </ns1:SnqcConsultarNotasExameRecertificacaoJSON>" + Router.soapMessageSuffix
      mutableURLRequest.addValue("http://tempuri.org/SnqcConsultarNotasExameRecertificacaoJSON", forHTTPHeaderField: "SOAPAction")
      mutableURLRequest.addValue(String(soapMessage.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)), forHTTPHeaderField: "Content-Length")
      mutableURLRequest.HTTPBody = soapMessage.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
    case .Certificates(let userId):
      let soapMessage = Router.soapMessagePrefix + "<ns1:SnqcConsultarExtratoJSON> <ns1:numero_snqc>\(userId)</ns1:numero_snqc> </ns1:SnqcConsultarExtratoJSON>" + Router.soapMessageSuffix
      mutableURLRequest.addValue("http://tempuri.org/SnqcConsultarExtratoJSON", forHTTPHeaderField: "SOAPAction")
      mutableURLRequest.addValue(String(soapMessage.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)), forHTTPHeaderField: "Content-Length")
      mutableURLRequest.HTTPBody = soapMessage.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
    case .Invoices(let userId):
      let soapMessage = Router.soapMessagePrefix + "<ns1:CobrancaConsultarBoletoPorUsuarioJSON> <ns1:codigo_snqc>\(userId)</ns1:codigo_snqc> </ns1:CobrancaConsultarBoletoPorUsuarioJSON>" + Router.soapMessageSuffix
      mutableURLRequest.addValue("http://tempuri.org/CobrancaConsultarBoletoPorUsuarioJSON", forHTTPHeaderField: "SOAPAction")
      mutableURLRequest.addValue(String(soapMessage.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)), forHTTPHeaderField: "Content-Length")
      mutableURLRequest.HTTPBody = soapMessage.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
    default:
      break
    }
    return mutableURLRequest
  }
  
  var elementPrefix: String {
    switch self {
    case .Search:
      return "SnqcConsultaPublica"
    case .ListTechniques:
      return "SNQCListarTecnicas"
    case .Login:
      return "Logar"
    case .AgendasTheoric:
      return "SnqcConsultarAgendaExameTeorico"
    case .AgendasPractical:
      return "SnqcConsultarAgendaExamePratico"
    case .AgendasRecertification:
      return "SnqcConsultarAgendaExameRecertificacao"
    case .GradesTheoric:
      return "SnqcConsultarNotasExameTeorico"
    case .GradesPractical:
      return "SnqcConsultarNotasExamePratico"
    case .GradesRecertification:
      return "SnqcConsultarNotasExameRecertificacao"
    case .Certificates:
      return "SnqcConsultarExtrato"
    case .Invoices:
      return "CobrancaConsultarBoletoPorUsuario"
    default:
      return ""
    }
  }
  
  func parse(objects: AnyObject) -> AnyObject {
    switch self {
    case .Search(let type, let value):
      if let results = objects as? [[String: AnyObject]] {
        if type == "snqc" {
          return Snqc(results)
        } else {
          return results.map() {
            Snqc($0)
          }
        }
      }
    case .ListTechniques:
      return objects
    case .Login:
      if let results = objects as? [String: AnyObject] {
        return ["login": results["login_snqc"] ?? "", "token": results["chave_seguranca"] ?? "", "name": results["nome"] ?? ""]
      }
    case .AgendasTheoric, .AgendasPractical, .AgendasRecertification:
      if let results = objects as? [[String: AnyObject]] {
        return results.map() {
          Agenda($0)
        }
      }
    case .GradesTheoric, .GradesPractical, .GradesRecertification:
      if let results = objects as? [[String: AnyObject]] {
        return results.map() {
          Grade($0)
        }
      }
    case .Certificates:
      if let results = objects as? [[String: AnyObject]] {
        return results.map() {
          Certificate($0)
        }
      }
    case .Invoices:
      if let results = objects as? [[String: AnyObject]] {
        return results.map() {
          Invoice($0)
        }
      }
    default:
      break
    }
    return []
  }
}

// MARK: API
extension NetworkManager {
  class func request(router: Router, completion: Result<AnyObject> -> ()) {
    let operation = AFHTTPRequestOperation(request: router.URLRequest)
    operation.setCompletionBlockWithSuccess({ operation, response in
      let parser = NSXMLParser(data: response as! NSData)
      let parserDelegate = XMLParser(router.elementPrefix)
      parser.delegate = parserDelegate
      parser.parse()
      dispatch_async(dispatch_get_main_queue()) {
        completion(Result(router.parse(parserDelegate.result)))
      }
      }) { operation, error in
        dispatch_async(dispatch_get_main_queue()) {
          completion(Result(Error(code: error.code, domain: error.domain, userInfo: ["description": error.localizedDescription])))
        }
    }
    NSOperationQueue.mainQueue().addOperation(operation)
  }
}
