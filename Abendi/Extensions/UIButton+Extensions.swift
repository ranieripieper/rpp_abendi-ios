//
//  UIButton+Extensions.swift
//  Abendi
//
//  Created by Gilson Gil on 3/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

extension UIButton {
  class func buttonWithTitle(title: String) -> UIButton {
    let button = UIButton.buttonWithType(.Custom) as! UIButton
    button.backgroundColor = UIColor.buttonBackgroundColor()
    let normalAttributed = NSAttributedString(string: title, attributes: [NSForegroundColorAttributeName: UIColor.buttonTitleColor(), NSFontAttributeName: UIFont(name: "Arial", size: 16)!])
    let selectedAttributed = NSAttributedString(string: title, attributes: [NSForegroundColorAttributeName: UIColor.buttonTitleColor(), NSFontAttributeName: UIFont(name: "Arial-BoldMT", size: 16)!])
    button.setAttributedTitle(normalAttributed, forState: .Normal)
    button.setAttributedTitle(selectedAttributed, forState: .Selected)
    return button
  }
}