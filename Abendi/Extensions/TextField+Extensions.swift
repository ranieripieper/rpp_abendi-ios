//
//  TextField+Extensions.swift
//  Abendi
//
//  Created by Gilson Gil on 3/10/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

extension UITextField {
  func setPaddings() {
    setPaddings(10)
  }
  
  func setPaddings(width: CGFloat) {
    leftView = UIView (frame: CGRect (origin: CGPointZero, size: CGSize (width: width, height: bounds.height)))
    leftViewMode = .Always
    rightView = UIView (frame: CGRect (origin: CGPointZero, size: CGSize (width: width - 4, height: bounds.height)))
    rightViewMode = .Always
  }
}
