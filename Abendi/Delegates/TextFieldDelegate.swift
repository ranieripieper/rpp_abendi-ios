//
//  TextFieldDelegate.swift
//  Abendi
//
//  Created by Gilson Gil on 4/16/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class TextFieldDelegate: NSObject {
  @IBOutlet weak var viewController: UIViewController!
}

extension TextFieldDelegate: UITextFieldDelegate {
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    if let nextTextField = viewController.view.viewWithTag(textField.tag + 1) {
      nextTextField.becomeFirstResponder()
    } else {
      textField.resignFirstResponder()
    }
    return true
  }
}
