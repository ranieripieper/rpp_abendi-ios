//
//  CertificatesTableViewDelegate.swift
//  Abendi
//
//  Created by Gilson Gil on 3/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CertificatesTableViewDelegate: NSObject {
  @IBOutlet weak var certificatesViewController: CertificatesViewController!
  
  var datasource = [Certificate]()
  var onceToken: dispatch_once_t = 0
  var sizingCell: CertificateCell?
}

extension CertificatesTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return datasource.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let certificateCell = tableView.dequeueReusableCellWithIdentifier("CertificateCell", forIndexPath: indexPath) as! CertificateCell
    certificateCell.configureWithCertificate(datasource[indexPath.row], darkBackground: indexPath.row % 2 == 0)
    return certificateCell
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    dispatch_once(&onceToken) {
      self.sizingCell = tableView.dequeueReusableCellWithIdentifier("CertificateCell") as? CertificateCell
    }
    sizingCell!.configureWithCertificate(datasource[indexPath.row], darkBackground: false)
    return calculateHeightForConfiguredCell(sizingCell!)
  }
  
  func calculateHeightForConfiguredCell(cell: UITableViewCell) -> CGFloat {
    cell.setNeedsUpdateConstraints()
    cell.updateConstraintsIfNeeded()
    cell.bounds = CGRect(x: 0.0, y: 0.0, width: UIScreen.mainScreen().bounds.width, height: cell.bounds.height)
    cell.setNeedsLayout()
    cell.layoutIfNeeded()
    return cell.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize).height + 1
  }
}