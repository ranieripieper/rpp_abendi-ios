//
//  SearchResultDetailTableViewDelegate.swift
//  Abendi
//
//  Created by Gilson Gil on 4/19/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SearchResultDetailTableViewDelegate: NSObject {
  @IBOutlet weak var searchResultDetailViewController: SearchResultDetailViewController!
  
  var dateFormatter: NSDateFormatter = {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy"
    return dateFormatter
  }()
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
}

extension SearchResultDetailTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 2
  }
  
  func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 55
  }
  
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let header = tableView.dequeueReusableHeaderFooterViewWithIdentifier("Header") as! Header
    switch section {
    case 0:
      header.configureWithText("DECLARAÇÕES DE CERTIFICAÇÕES VIGENTES - PROFISSIONAL CERTIFICADO")
    case 1:
      header.configureWithText("QUALIFICAÇÕES VIGENTES")
    case 2:
      header.configureWithText("QUALIFICAÇÕES VIGENTES - TRAINEE")
    case 3:
      header.configureWithText("EM FASE DE RECERTIFICAÇÃO OU RENOVAÇÃO")
    case 4:
      header.configureWithText("EM FASE DE RECONHECIMENTO")
    case 5:
      header.configureWithText("EM PROCESSO")
    default:
      break
    }
    return header
  }
  
  func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 30
  }
  
  func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    let footer = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 30))
    let line = UIView(frame: CGRect(x: 0, y: 29, width: tableView.bounds.width, height: 1))
    line.backgroundColor = UIColor(white: 0.8, alpha: 1)
    footer.addSubview(line)
    return footer
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch section {
    case 0:
      return 1
    case 1:
      return searchResultDetailViewController.snqc.currentCertificates().count + 1
    case 2:
      return searchResultDetailViewController.snqc.currentTraineeCertificates().count + 2
    case 3:
      return searchResultDetailViewController.snqc.recertificationRenovationCertificates().count + 1
    case 4:
      return searchResultDetailViewController.snqc.admissionCertificates().count + 1
    case 5:
      return searchResultDetailViewController.snqc.inProcessCertificates().count + 1
    default:
      return 0
    }
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    switch (indexPath.section, indexPath.row) {
    case (0, _):
      return 146
    case (1, let row):
      if searchResultDetailViewController.snqc.currentCertificates().count == 0 {
        return 40
      } else {
        if row == 0 {
          return 24
        } else {
          return 40
        }
      }
    case (2, let row):
      if searchResultDetailViewController.snqc.currentTraineeCertificates().count == 0 {
        if row == 0 {
          return 40
        } else {
          return 150
        }
      } else {
        if row == 0 {
          return 24
        } else {
          return 40
        }
      }
    case (3, let row):
      if searchResultDetailViewController.snqc.recertificationRenovationCertificates().count == 0 {
        return 40
      } else {
        if row == 0 {
          return 24
        } else {
          return 60
        }
      }
    case (4, let row):
      if searchResultDetailViewController.snqc.admissionCertificates().count == 0 {
        return 40
      } else {
        if row == 0 {
          return 24
        } else {
          return 60
        }
      }
    case (5, let row):
      if searchResultDetailViewController.snqc.inProcessCertificates().count == 0 {
        return 40
      } else {
        if row == 0 {
          return 24
        } else {
          return 60
        }
      }
    default:
      return 0
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    switch (indexPath.section, indexPath.row) {
    case (0, _):
      let cell = tableView.dequeueReusableCellWithIdentifier("SearchSNQCPersonCell", forIndexPath: indexPath) as! SearchSNQCPersonCell
      cell.configureWithSnqc(searchResultDetailViewController.snqc)
      return cell
    case (1, let row):
      let currentCertificates = searchResultDetailViewController.snqc.currentCertificates()
      if currentCertificates.count == 0 {
        let cell = tableView.dequeueReusableCellWithIdentifier("SearchSNQCEmptyCell", forIndexPath: indexPath) as! UITableViewCell
        return cell
      } else {
        if row == 0 {
          let cell = tableView.dequeueReusableCellWithIdentifier("SearchSNQCCurrentTitleCell", forIndexPath: indexPath) as! UITableViewCell
          return cell
        } else {
          let cell = tableView.dequeueReusableCellWithIdentifier("SearchSNQCDetailCell", forIndexPath: indexPath) as! SearchSNQCDetailCell
          cell.configureWithTechnique(currentCertificates[row - 1].technique, description: dateFormatter.stringFromDate(currentCertificates[row - 1].expiration!), darkBackground: row % 2 == 1)
          return cell
        }
      }
    case (2, let row):
      let currentTraineeCertificates = searchResultDetailViewController.snqc.currentTraineeCertificates()
      if row == currentTraineeCertificates.count + 1 {
        let cell = tableView.dequeueReusableCellWithIdentifier("SearchSNQCTraineeCell", forIndexPath: indexPath) as! UITableViewCell
        return cell
      } else if currentTraineeCertificates.count == 0 {
        let cell = tableView.dequeueReusableCellWithIdentifier("SearchSNQCEmptyCell", forIndexPath: indexPath) as! UITableViewCell
        return cell
      } else {
        if row == 0 {
          let cell = tableView.dequeueReusableCellWithIdentifier("SearchSNQCCurrentTitleCell", forIndexPath: indexPath) as! UITableViewCell
          return cell
        } else {
          let cell = tableView.dequeueReusableCellWithIdentifier("SearchSNQCDetailCell", forIndexPath: indexPath) as! SearchSNQCDetailCell
          cell.configureWithTechnique(currentTraineeCertificates[row - 1].technique, description: dateFormatter.stringFromDate(currentTraineeCertificates[row - 1].expiration!), darkBackground: row % 2 == 1)
          return cell
        }
      }
    case (3, let row):
      let recertificationRenovationCertificates = searchResultDetailViewController.snqc.recertificationRenovationCertificates()
      if recertificationRenovationCertificates.count == 0 {
        let cell = tableView.dequeueReusableCellWithIdentifier("SearchSNQCEmptyCell", forIndexPath: indexPath) as! UITableViewCell
        return cell
      } else {
        if row == 0 {
          let cell = tableView.dequeueReusableCellWithIdentifier("SearchSNQCNonCurrentTitleCell", forIndexPath: indexPath) as! UITableViewCell
          return cell
        } else {
          let cell = tableView.dequeueReusableCellWithIdentifier("SearchSNQCDetailCell", forIndexPath: indexPath) as! SearchSNQCDetailCell
          cell.configureWithTechnique(recertificationRenovationCertificates[row - 1].technique, description: recertificationRenovationCertificates[row - 1].description, darkBackground: row % 2 == 1)
          return cell
        }
      }
    case (4, let row):
      let admissionCertificates = searchResultDetailViewController.snqc.admissionCertificates()
      if admissionCertificates.count == 0 {
        let cell = tableView.dequeueReusableCellWithIdentifier("SearchSNQCEmptyCell", forIndexPath: indexPath) as! UITableViewCell
        return cell
      } else {
        if row == 0 {
          let cell = tableView.dequeueReusableCellWithIdentifier("SearchSNQCNonCurrentTitleCell", forIndexPath: indexPath) as! UITableViewCell
          return cell
        } else {
          let cell = tableView.dequeueReusableCellWithIdentifier("SearchSNQCDetailCell", forIndexPath: indexPath) as! SearchSNQCDetailCell
          cell.configureWithTechnique(admissionCertificates[row - 1].technique, description: admissionCertificates[row - 1].description, darkBackground: row % 2 == 1)
          return cell
        }
      }
    case (5, let row):
      let inProcessCertificates = searchResultDetailViewController.snqc.inProcessCertificates()
      if inProcessCertificates.count == 0 {
        let cell = tableView.dequeueReusableCellWithIdentifier("SearchSNQCEmptyCell", forIndexPath: indexPath) as! UITableViewCell
        return cell
      } else {
        if row == 0 {
          let cell = tableView.dequeueReusableCellWithIdentifier("SearchSNQCNonCurrentTitleCell", forIndexPath: indexPath) as! UITableViewCell
          return cell
        } else {
          let cell = tableView.dequeueReusableCellWithIdentifier("SearchSNQCDetailCell", forIndexPath: indexPath) as! SearchSNQCDetailCell
          cell.configureWithTechnique(inProcessCertificates[row - 1].technique, description: inProcessCertificates[row - 1].description, darkBackground: row % 2 == 1)
          return cell
        }
      }
    default:
      return UITableViewCell()
    }
  }
}
