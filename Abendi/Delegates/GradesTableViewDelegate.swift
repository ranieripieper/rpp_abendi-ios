//
//  GradesTableViewDelegate.swift
//  Abendi
//
//  Created by Gilson Gil on 3/25/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

typealias GradesViewType = (Type, [Grade])

class GradesTableViewDelegate: NSObject {
  @IBOutlet weak var gradesViewController: GradesViewController!
  
  var datasource: [GradesViewType] = [(.Theoric, []), (.Practical, []), (.Recertification, [])] {
    didSet {
      filteredDatasource = datasource
    }
  }
  var filteredDatasource: [GradesViewType]!
  let dateFormatter = NSDateFormatter()
  
  override func awakeFromNib() {
    super.awakeFromNib()
    dateFormatter.dateFormat = "dd/MM/yyyy"
    filteredDatasource = datasource
  }
}

extension GradesTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return filteredDatasource.count
  }
  
  func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 80
  }
  
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let header = tableView.dequeueReusableHeaderFooterViewWithIdentifier("Header") as! Header
    header.configureWithText(filteredDatasource[section].0.toGradesString())
    return header
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return max(filteredDatasource[section].1.count, 1)
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if filteredDatasource[indexPath.section].1.count > 0 {
      switch filteredDatasource[indexPath.section].0 {
      case .Theoric:
        return 162
      case .Practical:
        return 233
      case .Recertification:
        return 149
      default:
        return 0
      }
    } else {
      return 140
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    if filteredDatasource[indexPath.section].1.count > 0 {
      switch filteredDatasource[indexPath.section].0 {
      case .Theoric:
        let gradeCell = tableView.dequeueReusableCellWithIdentifier("GradeTheoricCell", forIndexPath: indexPath) as! GradeTheoricCell
        let grade = filteredDatasource[indexPath.section].1[indexPath.row]
        gradeCell.configureWithGrade(grade, dateFormatter: dateFormatter)
        return gradeCell
      case .Practical:
        let gradeCell = tableView.dequeueReusableCellWithIdentifier("GradePracticalCell", forIndexPath: indexPath) as! GradePracticalCell
        let grade = filteredDatasource[indexPath.section].1[indexPath.row]
        gradeCell.configureWithGrade(grade, dateFormatter: dateFormatter)
        return gradeCell
      case .Recertification:
        let gradeCell = tableView.dequeueReusableCellWithIdentifier("GradeRecertificationCell", forIndexPath: indexPath) as! GradeRecertificationCell
        let grade = filteredDatasource[indexPath.section].1[indexPath.row]
        gradeCell.configureWithGrade(grade, dateFormatter: dateFormatter)
        return gradeCell
      default:
        return UITableViewCell()
      }
    } else {
      let gradeEmptyCell = tableView.dequeueReusableCellWithIdentifier("GradeEmptyCell", forIndexPath: indexPath) as! GradeEmptyCell
      gradeEmptyCell.configureWithType(filteredDatasource[indexPath.section].0)
      return gradeEmptyCell
    }
  }
}
