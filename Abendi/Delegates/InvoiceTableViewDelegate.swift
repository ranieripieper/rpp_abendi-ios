//
//  InvoiceTableViewDelegate.swift
//  Abendi
//
//  Created by Gilson Gil on 3/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class InvoiceTableViewDelegate: NSObject {
  @IBOutlet weak var invoicesViewController: InvoicesViewController!
  @IBOutlet weak var tableView: UITableView!
  
  var datasource = [Invoice]()
  var dateFormatter: NSDateFormatter {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy"
    return dateFormatter
  }
  var onceToken: dispatch_once_t = 0
  var sizingCell: InvoiceCell?
  
  func pay(invoiceCell: InvoiceCell) {
    if let row = tableView.indexPathForCell(invoiceCell)?.row {
      let invoice = datasource[row]
      invoicesViewController.payInvoice(invoice)
    }
  }
}

extension InvoiceTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return datasource.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let invoiceCell = tableView.dequeueReusableCellWithIdentifier("InvoiceCell", forIndexPath: indexPath) as! InvoiceCell
    invoiceCell.configureWithInvoice(datasource[indexPath.row], dateFormatter: dateFormatter, payCallback: pay)
    return invoiceCell
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    dispatch_once(&onceToken) {
      self.sizingCell = tableView.dequeueReusableCellWithIdentifier("InvoiceCell") as? InvoiceCell
    }
    sizingCell!.configureWithInvoice(datasource[indexPath.row], dateFormatter: dateFormatter, payCallback: nil)
    return calculateHeightForConfiguredCell(sizingCell!)
  }
  
  func calculateHeightForConfiguredCell(cell: UITableViewCell) -> CGFloat {
    cell.setNeedsUpdateConstraints()
    cell.updateConstraintsIfNeeded()
    cell.bounds = CGRect(x: 0.0, y: 0.0, width: UIScreen.mainScreen().bounds.width, height: cell.bounds.height)
    cell.setNeedsLayout()
    cell.layoutIfNeeded()
    return cell.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize).height + 1
  }
}