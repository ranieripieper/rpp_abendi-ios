//
//  SearchResultsTableViewDelegate.swift
//  Abendi
//
//  Created by Gilson Gil on 3/10/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SearchResultsTableViewDelegate: NSObject {
  @IBOutlet weak var searchResultsViewController: SearchResultsViewController!
  
  var datasource = [Snqc]()
  var dateFormatter: NSDateFormatter = {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy"
    return dateFormatter
  }()
}

extension SearchResultsTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 44
  }
  
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let searchResultsHeader = tableView.dequeueReusableHeaderFooterViewWithIdentifier("SearchResultsHeader") as! SearchResultsHeader
    searchResultsHeader.configureWithTotal(datasource.count)
    return searchResultsHeader
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return datasource.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let searchResultCell = tableView.dequeueReusableCellWithIdentifier("SearchResultCell", forIndexPath: indexPath) as! SearchResultCell
    searchResultCell.configureWithSNQC(datasource[indexPath.row], dateFormatter)
    return searchResultCell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    searchResultsViewController.searchSnqc(datasource[indexPath.row])
  }
}
