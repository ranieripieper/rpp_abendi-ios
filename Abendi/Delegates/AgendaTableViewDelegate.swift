//
//  AgendaTableViewDelegate.swift
//  Abendi
//
//  Created by Gilson Gil on 3/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

typealias AgendaViewType = (Type, [Agenda])

class AgendaTableViewDelegate: NSObject {
  @IBOutlet weak var agendaViewController: AgendaViewController!
  
  var datasource: [AgendaViewType] = [(.Theoric, []), (.Practical, []), (.Recertification, [])] {
    didSet {
      filteredDatasource = datasource
    }
  }
  var filteredDatasource: [AgendaViewType]!
  var onceToken: dispatch_once_t = 0
  var sizingCell: AgendaCell?
  var onceEmptyToken: dispatch_once_t = 0
  var sizingEmptyCell: AgendaEmptyCell?
  let dateFormatter = NSDateFormatter()
  
  override func awakeFromNib() {
    super.awakeFromNib()
    dateFormatter.dateFormat = "dd/MM/yyyy"
    filteredDatasource = datasource
  }
}

extension AgendaTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return filteredDatasource.count
  }
  
  func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 80
  }
  
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let header = tableView.dequeueReusableHeaderFooterViewWithIdentifier("Header") as! Header
    header.configureWithText(filteredDatasource[section].0.toAgendaString())
    return header
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return max(filteredDatasource[section].1.count, 1)
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    if filteredDatasource[indexPath.section].1.count > 0 {
      let agendaCell = tableView.dequeueReusableCellWithIdentifier("AgendaCell", forIndexPath: indexPath) as! AgendaCell
      let agenda = filteredDatasource[indexPath.section].1[indexPath.row]
      agendaCell.configureWithAgenda(agenda, dateFormatter: dateFormatter)
      return agendaCell
    } else {
      let agendaEmptyCell = tableView.dequeueReusableCellWithIdentifier("AgendaEmptyCell", forIndexPath: indexPath) as! AgendaEmptyCell
      agendaEmptyCell.configureWithType(filteredDatasource[indexPath.section].0)
      return agendaEmptyCell
    }
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if filteredDatasource[indexPath.section].1.count > 0 {
      dispatch_once(&onceToken) {
        self.sizingCell = tableView.dequeueReusableCellWithIdentifier("AgendaCell") as? AgendaCell
      }
      let agenda = filteredDatasource[indexPath.section].1[indexPath.row]
      sizingCell!.configureWithAgenda(agenda, dateFormatter: dateFormatter)
      return calculateHeightForConfiguredCell(sizingCell!)
    } else {
      dispatch_once(&onceEmptyToken) {
        self.sizingEmptyCell = tableView.dequeueReusableCellWithIdentifier("AgendaEmptyCell") as? AgendaEmptyCell
      }
      sizingEmptyCell!.configureWithType(filteredDatasource[indexPath.section].0)
      return calculateHeightForConfiguredCell(sizingEmptyCell!) + 40
    }
  }
  
  func calculateHeightForConfiguredCell(cell: UITableViewCell) -> CGFloat {
    cell.setNeedsUpdateConstraints()
    cell.updateConstraintsIfNeeded()
    cell.bounds = CGRect(x: 0.0, y: 0.0, width: UIScreen.mainScreen().bounds.width, height: cell.bounds.height)
    cell.setNeedsLayout()
    cell.layoutIfNeeded()
    return cell.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize).height + 1
  }
}