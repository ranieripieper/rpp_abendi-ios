//
//  Constants.swift
//  Abendi
//
//  Created by Gilson Gil on 3/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

extension UIColor {
  // MARK: Text Color
  class func appDarkTextColor() -> UIColor {
    return UIColor(red: 52.0/255.0, green: 52.0/255.0, blue: 52.0/255.0, alpha: 1)
  }
  
  class func appLightTextColor() -> UIColor {
    return UIColor(red: 125.0/255.0, green: 125.0/255.0, blue: 125.0/255.0, alpha: 1)
  }
  
  class func lightestTextColor() -> UIColor {
    return UIColor(red: 180/255, green: 180/255, blue: 180/255, alpha: 1)
  }
  
  // MARK: Tab Bar Color
  class func tabbarColor() -> UIColor {
    return UIColor(red: 92/255, green: 92/255, blue: 92/255, alpha: 1)
  }
  
  class func selectedTabbarColor() -> UIColor {
    return UIColor(red: 52/255, green: 52/255, blue: 52/255, alpha: 1)
  }
  
  // MARK: Filter Color
  class func buttonBackgroundColor() -> UIColor {
    return UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1)
  }
  
  class func buttonTitleColor() -> UIColor {
    return UIColor(red: 92/255, green: 92/255, blue: 92/255, alpha: 1)
  }
  
  // MARK: General
  class func appYellowColor() -> UIColor {
    return UIColor(red: 232.0/255.0, green: 175.0/255.0, blue: 34.0/255.0, alpha: 1)
  }
}
