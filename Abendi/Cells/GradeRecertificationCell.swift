//
//  GradeRecertificationCell.swift
//  Abendi
//
//  Created by Gilson Gil on 4/17/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class GradeRecertificationCell: UITableViewCell {
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var techniqueLabel: UILabel!
  @IBOutlet weak var grade1Label: UILabel!
  @IBOutlet weak var grade2Label: UILabel!
  @IBOutlet weak var resultLabel: UILabel!
  
  func configureWithGrade(grade: Grade, dateFormatter: NSDateFormatter) {
    dateLabel.text = dateFormatter.stringFromDate(grade.date)
    techniqueLabel.text = grade.technique
    if let grade1 = grade.grade1 where count(grade1) > 0 {
      grade1Label.text = grade1
    } else {
      grade1Label.text = "-"
    }
    if let grade2 = grade.grade2 where count(grade2) > 0 {
      grade2Label.text = grade2
    } else {
      grade2Label.text = "-"
    }
    resultLabel.text = grade.grade
  }
}
