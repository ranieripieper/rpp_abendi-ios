//
//  SearchSNQCPersonCell.swift
//  Abendi
//
//  Created by Gilson Gil on 4/19/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SearchSNQCPersonCell: UITableViewCell {
  @IBOutlet weak var snqcNumberLabel: UILabel!
  @IBOutlet weak var namePersonLabel: UILabel!
  @IBOutlet weak var emissionDateLabel: UILabel!
  
  var dateFormatter: NSDateFormatter = {
    let dateFormatter = NSDateFormatter()
    dateFormatter.locale = NSLocale(localeIdentifier: "pt-BR")
    dateFormatter.dateFormat = "EEEE, dd 'de' MMMM 'de' yyyy"
    return dateFormatter
  }()
  
  override func awakeFromNib() {
    super.awakeFromNib()
    snqcNumberLabel.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.width - 12 * 2
    namePersonLabel.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.width - 12 * 2
    emissionDateLabel.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.width - 12 * 2
  }
  
  func configureWithSnqc(snqc: Snqc) {
    snqcNumberLabel.text = String(snqc.snqc)
    namePersonLabel.text = snqc.name.capitalizedString
    emissionDateLabel.text = dateFormatter.stringFromDate(NSDate()).lowercaseString
  }
}
