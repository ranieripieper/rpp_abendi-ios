//
//  GradeEmptyCell.swift
//  Abendi
//
//  Created by Gilson Gil on 3/25/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class GradeEmptyCell: UITableViewCell {
  @IBOutlet weak var emptyLabel: UILabel!
  
  func configureWithType(type: Type) {
    emptyLabel.text = type.toGradesDescriptionString()
  }
}
