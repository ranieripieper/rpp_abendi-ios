//
//  GradeTheoricCell.swift
//  Abendi
//
//  Created by Gilson Gil on 3/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class GradeTheoricCell: UITableViewCell {
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var techniqueLabel: UILabel!
  @IBOutlet weak var specificGradeLabel: UILabel!
  @IBOutlet weak var generalGradeLabel: UILabel!
  @IBOutlet weak var resultLabel: UILabel!
  
  func configureWithGrade(grade: Grade, dateFormatter: NSDateFormatter) {
    dateLabel.text = dateFormatter.stringFromDate(grade.date)
    techniqueLabel.text = grade.technique
    if let specificGrade = grade.specificGrade where count(specificGrade) > 0 {
      specificGradeLabel.text = specificGrade
    } else {
      specificGradeLabel.text = "-"
    }
    if let generalGrade = grade.generalGrade where count(generalGrade) > 0 {
      generalGradeLabel.text = generalGrade
    } else {
      generalGradeLabel.text = "-"
    }
    resultLabel.text = grade.grade
  }
}
