//
//  SearchResultCell.swift
//  Abendi
//
//  Created by Gilson Gil on 3/10/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SearchResultCell: UITableViewCell {
  @IBOutlet weak var snqcLabel: UILabel!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var techniquesLabel: UILabel!
  @IBOutlet weak var expirationLabel: UILabel!
  @IBOutlet weak var locationLabel: UILabel!
  
  func configureWithSNQC(snqc: Snqc, _ dateFormatter: NSDateFormatter) {
    snqcLabel.text = String(snqc.snqc)
    nameLabel.text = snqc.name
    techniquesLabel.text = snqc.certificates.first?.technique
    expirationLabel.text = dateFormatter.stringFromDate(snqc.certificates.first?.expiration ?? NSDate(timeIntervalSince1970: 0))
    locationLabel.text = snqc.location
  }
}
