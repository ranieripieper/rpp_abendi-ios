//
//  InvoiceCell.swift
//  Abendi
//
//  Created by Gilson Gil on 3/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class InvoiceCell: UITableViewCell {
  typealias payCallbackType = ((InvoiceCell) -> ())
  
  @IBOutlet weak var expirationLabel: UILabel!
  @IBOutlet weak var faceValueLabel: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!
  @IBOutlet weak var payButton: UIButton!
  
  var payCallback: payCallbackType?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    descriptionLabel.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.width - 12 * 2
  }
  
  func configureWithInvoice(invoice: Invoice, dateFormatter: NSDateFormatter, payCallback: payCallbackType?) {
    expirationLabel.text = dateFormatter.stringFromDate(invoice.expirationDate)
    faceValueLabel.text = invoice.faceValue
    descriptionLabel.text = invoice.description
    self.payCallback = payCallback
  }
  
  @IBAction func pay(sender: UIButton) {
    payCallback?(self)
  }
}
