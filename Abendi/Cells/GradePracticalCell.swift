//
//  GradePracticalCell.swift
//  Abendi
//
//  Created by Gilson Gil on 4/17/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class GradePracticalCell: UITableViewCell {
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var techniqueLabel: UILabel!
  @IBOutlet weak var step1GradeLabel: UILabel!
  @IBOutlet weak var step1AverageGradeLabel: UILabel!
  @IBOutlet weak var step2GradeLabel: UILabel!
  @IBOutlet weak var step2AverageGradeLabel: UILabel!
  @IBOutlet weak var step3GradeLabel: UILabel!
  @IBOutlet weak var step3AverageGradeLabel: UILabel!
  @IBOutlet weak var resultLabel: UILabel!
  
  func configureWithGrade(grade: Grade, dateFormatter: NSDateFormatter) {
    dateLabel.text = dateFormatter.stringFromDate(grade.date)
    techniqueLabel.text = grade.technique
    if let step1Grade = grade.step1Grade where count(step1Grade) > 0 {
      step1GradeLabel.text = step1Grade
    } else {
      step1GradeLabel.text = "-"
    }
    if let step1AverageGrade = grade.step1AverageGrade where count(step1AverageGrade) > 0 {
      step1AverageGradeLabel.text = step1AverageGrade
    } else {
      step1AverageGradeLabel.text = "-"
    }
    if let step2Grade = grade.step2Grade where count(step2Grade) > 0 {
      step2GradeLabel.text = step2Grade
    } else {
      step2GradeLabel.text = "-"
    }
    if let step2AverageGrade = grade.step2AverageGrade where count(step2AverageGrade) > 0 {
      step2AverageGradeLabel.text = step2AverageGrade
    } else {
      step2AverageGradeLabel.text = "-"
    }
    if let step3Grade = grade.step3Grade where count(step3Grade) > 0 {
      step3GradeLabel.text = step3Grade
    } else {
      step3GradeLabel.text = "-"
    }
    if let step3AverageGrade = grade.step3AverageGrade where count(step3AverageGrade) > 0 {
      step3AverageGradeLabel.text = step3AverageGrade
    } else {
      step3AverageGradeLabel.text = "-"
    }
    resultLabel.text = grade.grade
  }
}
