//
//  SearchSNQCDetailCell.swift
//  Abendi
//
//  Created by Gilson Gil on 4/19/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SearchSNQCDetailCell: UITableViewCell {
  @IBOutlet weak var techniqueLabel: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!
  
  func configureWithTechnique(technique: String, description: String, darkBackground: Bool) {
    techniqueLabel.text = technique
    descriptionLabel.text = description
    if darkBackground {
      contentView.backgroundColor = UIColor(white: 0.94, alpha: 1.0)
    } else {
      contentView.backgroundColor = UIColor.whiteColor()
    }
  }
}
