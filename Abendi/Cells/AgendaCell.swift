//
//  AgendaCell.swift
//  Abendi
//
//  Created by Gilson Gil on 3/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class AgendaCell: UITableViewCell {
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var techniqueLabel: UILabel!
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var locationLabel: UILabel!
  
  func configureWithAgenda(agenda: Agenda, dateFormatter: NSDateFormatter) {
    dateLabel.text = dateFormatter.stringFromDate(agenda.date)
    techniqueLabel.text = agenda.technique
    timeLabel.text = agenda.time
    locationLabel.text = agenda.location
  }
}
