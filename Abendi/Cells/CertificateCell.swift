//
//  CertificateCell.swift
//  Abendi
//
//  Created by Gilson Gil on 3/11/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CertificateCell: UITableViewCell {
  @IBOutlet weak var techniqueLabel: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    descriptionLabel.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.width * 0.4 - 20
  }
  
  func configureWithCertificate(certificate: Certificate, darkBackground: Bool) {
    techniqueLabel.text = certificate.technique
    descriptionLabel.text = certificate.description
    if darkBackground {
      contentView.backgroundColor = UIColor(white: 0.94, alpha: 1.0)
    } else {
      contentView.backgroundColor = UIColor.whiteColor()
    }
  }
}
